#function for calculating maximum
#Parameter 1 x: values from data1 list
#Parameter 2 current: current value in data1 discharge 
max_fun <- function(x, current){
  if (current > max) {
    max = current
  }
return(max)
}#END FUNCTION

#function for calculating standard deviation
#Parameter 1 x: input a list 
SD_fun <- function(x) {
  N = length(x)
  sum_num <-  sum((x - mean(x))^2)
  SD <-  sqrt(sum_num / (N - 1))
  return(SD)
}#END FUNCTION


#function for Log Pearson Type 3 
#Parameter 1 x: Input List
#Parameter 2 p: Input percentile
LP3_fun <- function(x, p) {
  y <- log(x)
  N <-  length(x)
  Zp <- qnorm(p)
  Skew <- (N * sum((y - mean(y))^3)) / ((N - 1) * (N - 2) * (sd(y)^3))
  Gy <- (1 + (6 / N)) * Skew
  Kp <- (2 / Gy) * ((1 + ((Gy * Zp) / 6) - (Gy^2 / 36))^3) - (2 / Gy)
  xp <- exp(mean(y) + Kp * sd(y))
  return(xp)
}#END FUNCTION


#function for 3 and 2 parameter Lognormal Distribution
#Parameter 1 x: Input List
#Parameter 2 p: Input percentile
LND_fun <- function (x, p){
  Zp <- qnorm(p)
  N <- length(x)
  x1 <- min(x)
  xN <- max(x)
  x_med <- median(x)
  if (x1 + xN < 2 * x_med){
    #2 parameter lognormal distribution
    y_bar <- sum(log(x)) / N
    SD <- sqrt((sum(log(x) - y_bar)^2) / (N-1))
    Q <- exp(y_bar + Zp * SD)
  }
  else {
    #3 parameter lognormal distribution
    E <- ((x1 * xN - (x_med)^2) / (x1 + xN - 2 * x_med))
    y <- log(x - E)
    y_bar <- sum(y)/N
    SD <- sqrt((sum((y - y_bar)^2)) / (N - 1))
    Q <- E + exp(y_bar + Zp * SD)
  }#END IF
  return(Q)
}#END FUNCTION


