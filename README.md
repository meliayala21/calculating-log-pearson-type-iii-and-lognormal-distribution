# Calculating Log Pearson type III and Lognormal Distribution

Calculated average stream discharge, maximum discharge, and the seven day annual minimum for each water year in the dataset. Log pearson type III calculated using max discharge and lognormal distribution calculated using seven day annual min.